"use client";
import React, { useState, useEffect, FormEvent } from "react";
import axios from "axios";
import { useRouter } from "next/navigation";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import { setProfile } from "../../redux/profileSlice";
import { setUser } from "../../redux/userSlice";
import DraggableItem from "./DraggableItem";
import DroppableArea from "./DroppableArea";
import Image from "next/image";

const titles = ["TO DO", "IN PROGRESS", "READY TO QA", "DONE"];

function Dashboard() {
  const router = useRouter();
  const currentUser: any = useSelector((state) => state);
  const dispatch = useDispatch();
  const [users, setUsers] = useState({ avatar: "", first_name: "" });
  const [areas, setAreas] = useState<{ id: number; items: string[] }[]>([
    { id: 1, items: [] },
    { id: 2, items: [] },
    { id: 3, items: [] },
    { id: 4, items: [] },
  ]);
  const [isDraggableVisible, setIsDraggableVisible] = useState(true);

  const handleAddNewDraggable = (title: string) => {
    if (title.trim() !== "") {
      const newDraggableItem = title;
      const updatedAreas = areas.map((area) => {
        if (area.id === 1) {
          return { ...area, items: [...area.items, newDraggableItem] };
        }
        return area;
      });

      setAreas(updatedAreas);
    }
  };

  const handleDrop = (item: string, areaId: number) => {
    const updatedAreas = areas.map((area) => {
      if (area.id === areaId) {
        return { ...area, items: [...area.items, item] };
      } else {
        return { ...area, items: area.items.filter((i) => i !== item) };
      }
    });

    setAreas(updatedAreas);
  };

  useEffect(() => {
    axios
      .post("/api/profile", {
        email: currentUser.user.currentUser.email,
      })
      .then((x) => {
        dispatch(setProfile(x.data[0]));
        setUsers(x.data[0]);
      })
      .catch((e) => console.error(e));
  }, []);

  const logout = async () => {
    try {
      const res = await axios.get("/api/auth/logout");
    } catch (error: any) {
      console.error(error.message);
    }
    router.push("/");
  };
  return (
    <div>
      {users ? (
        <div
          style={{
            display: "grid",
            gridAutoFlow: "row",
            padding: "64px",
          }}
        >
          <div
            className="navbar"
            style={{
              display: "grid",
              gridAutoFlow: "column",
              marginBottom: "30px",
            }}
          >
            <h1
              style={{
                fontSize: "64px",
                color: "#176B87",
              }}
            >
              Task Manager
            </h1>

            <div
              style={{
                display: "grid",
                gridAutoFlow: "column",
                justifyContent: "end",
                gap: "16px",
                alignItems: "center",
              }}
            >
              <Image
                src={users.avatar}
                alt={users.first_name}
                width={64}
                height={64}
                style={{ borderRadius: "50%" }}
              />
              <div onClick={() => logout()} className="customBtn">
                Logout
              </div>
            </div>
          </div>
          <hr
            style={{
              marginBottom: "8px",
              borderColor: "#DAFFFB",
              backgroundColor: "#DAFFFB",
              height: "1px",
              border: "none",
            }}
          />
          <div
            style={{
              marginBottom: "32px",
              display: "grid",
              gridAutoFlow: "column",
              gap: "16px",
              justifyContent: "flex-end",
            }}
          >
            <input
              type="text"
              placeholder="New Task Name"
              id="newTask"
              style={{ padding: "8px" }}
            />
            <button
              onClick={() => {
                const titleInput = document.getElementById(
                  "newTask"
                ) as HTMLInputElement;
                handleAddNewDraggable(titleInput.value);
                titleInput.value = "";
              }}
              style={{
                padding: "8px",
                height: "fit-content",
              }}
            >
              Add
            </button>
          </div>
          <div className="area-container">
            {areas.map((area, index) => (
              <DroppableArea
                key={area.id}
                areaId={area.id}
                items={area.items}
                onInitialDrop={(item) => handleDrop(item, area.id)}
                onDrop={(item) => handleDrop(item, area.id)}
                title={titles[index]}
              />
            ))}
          </div>
        </div>
      ) : null}
    </div>
  );
}

export default Dashboard;
