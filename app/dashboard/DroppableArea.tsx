import React, { useState } from "react";

interface DroppableAreaProps {
  areaId: number;
  items: string[];
  onInitialDrop: (item: string) => void;
  onDrop: (item: string) => void;
  title: string;
}

const DroppableArea: React.FC<DroppableAreaProps> = ({
  areaId,
  items,
  onInitialDrop,
  onDrop,
  title,
}) => {
  const [isDropping, setIsDropping] = useState(false);

  const handleDragOver = (event: React.DragEvent<HTMLDivElement>) => {
    event.preventDefault();
    setIsDropping(true);
  };

  const handleDrop = (event: React.DragEvent<HTMLDivElement>) => {
    event.preventDefault();
    setIsDropping(false);
    const data = event.dataTransfer.getData("text/plain");
    if (data) {
      if (items.length === 0) {
        onInitialDrop(data);
      } else {
        onDrop(data);
      }
    }
  };

  return (
    <div style={{ textAlign: "start" }}>
      <h2 style={{ marginLeft: "16px" }}>{title}</h2>
      <div
        className={`droppable-area ${isDropping ? "drag-over" : ""}`}
        style={{ width: "300px", height: "600px" }}
        onDragOver={handleDragOver}
        onDrop={handleDrop}
      >
        {items.map((item, index) => (
          <div
            key={index}
            className="dropped-item"
            draggable
            onDragStart={(e) => e.dataTransfer.setData("text/plain", item)}
          >
            {item}
          </div>
        ))}
      </div>
    </div>
  );
};

export default DroppableArea;
