import React from "react";

const DraggableItem: React.FC = () => {
  const handleDragStart = (event: React.DragEvent<HTMLDivElement>) => {
    event.dataTransfer.setData("text/plain", "dragging");
  };

  return (
    <div className="draggable-item" draggable onDragStart={handleDragStart}>
      Draggable Item
    </div>
  );
};

export default DraggableItem;
