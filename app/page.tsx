"use client";
import { FormEvent, useState } from "react";
import { AxiosError } from "axios";
import axios from "axios";
import { useRouter } from "next/navigation";
import { useDispatch } from "react-redux";
import { setUser } from "../redux/userSlice";
import { useSelector } from "react-redux";

function Signin() {
  const [error, setError] = useState("");
  const router = useRouter();
  const dispatch = useDispatch();
  const currentUser = useSelector((state) => state);

  const handleSubmit = async (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const formData = new FormData(event.currentTarget);
    await axios
      .post("/api/auth/login", {
        email: formData.get("email"),
        password: formData.get("password"),
      })
      .then((x) => {
        dispatch(
          setUser({ token: x.data.token, email: formData.get("email") })
        );
        router.push("/dashboard");
      });
  };

  return (
    <div
      style={{
        justifyContent: "center",
        alignContent: "center",
        height: "100vh",
        display: "grid",
        textAlign: "center",
      }}
    >
      <form onSubmit={handleSubmit} className="formContainer">
        {error && <div>{error}</div>}
        <h1 style={{ margin: "34px" }}>Login</h1>

        <div style={{ display: "grid", gap: "34px", margin: "64px" }}>
          <div
            style={{
              display: "grid",
              gridAutoFlow: " column",
              gap: "34px",
            }}
          >
            <input
              type="email"
              placeholder="Email"
              name="email"
              style={{
                padding: "8px",
              }}
            />
          </div>
          <div
            style={{
              display: "grid",
              gridAutoFlow: " column",
              gap: "34px",
            }}
          >
            <input
              type="password"
              placeholder="Password"
              name="password"
              style={{
                padding: "8px",
              }}
            />
          </div>
        </div>

        <button
          style={{
            padding: "8px",
          }}
        >
          Signin
        </button>
      </form>
    </div>
  );
}

export default Signin;
