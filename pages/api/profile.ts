"use server";

import { cookies } from "next/headers";
import { NextResponse } from "next/server";
import jwt from "jsonwebtoken";
import axios from "axios";

export default function handler(req: any, res: any) {
  const responseApi = axios
    .get("https://reqres.in/api/users?per_page=12")
    .then((x: any) => {
      const response = x.data.data.filter(
        (y: { email: any }) => y.email === req.body.email
      );

      return res.json(response);
    })
    .catch((e: any) => res.json(e));
}
