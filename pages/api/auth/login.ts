"use server";

import axios from "axios";
import { sign } from "jsonwebtoken";
import { NextResponse } from "next/server";

export default function handler(req: any, res: any) {
  const responseApi = axios
    .post("https://reqres.in/api/login", req.body)
    .then((x: any) => {
      const token = sign(
        {
          exp: Math.floor(Date.now() / 1000) + 60 * 60 * 24 * 30,
          email: req.body.email,
        },
        "secret"
      );

      const response = NextResponse.json({
        token,
      });

      response.cookies.set({
        name: "myTokenName",
        value: token,
        httpOnly: true,
        secure: false,
        sameSite: "strict",
        maxAge: 1000 * 60 * 60 * 24 * 30,
        path: "/",
      });

      return res.json(x.data);
    })
    .catch((e: any) => {
      return res.json({
        status: 401,
      });
    });

  return responseApi;
}
