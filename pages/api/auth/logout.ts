import { NextResponse } from "next/server";
import { cookies } from "next/headers";

export default function handler(req: any, res: any) {
  const cookieStore = cookies();
  const token = cookieStore.get("myTokenName");

  if (!token) {
    return NextResponse.json(
      {
        message: "Not logged in",
      },
      {
        status: 401,
      }
    );
  }

  try {
    cookieStore.delete("myTokenName");

    const response = NextResponse.json(
      {},
      {
        status: 200,
      }
    );

    return response;
  } catch (error) {
    return NextResponse.json(error.message, {
      status: 500,
    });
  }
}
