import { configureStore } from "@reduxjs/toolkit";
import userReducer from "./userSlice";
import profileReducer from "./profileSlice"; // Crea este archivo en el mismo directorio

const store = configureStore({
  reducer: {
    user: userReducer,
    profile: profileReducer,
    // Agrega otros reducers aquí si es necesario
  },
});

export default store;
