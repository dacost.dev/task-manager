import { createSlice } from "@reduxjs/toolkit";

interface ProfileState {
  currentProfile: Profile | null;
}

interface Profile {
  avatar: string;
  email: string;
  first_name: string;
  id: number;
  last_name: string;
}

const initialState: ProfileState = {
  currentProfile: null,
};

const profileSlice = createSlice({
  name: "profile",
  initialState,
  reducers: {
    setProfile: (state, action) => {
      state.currentProfile = action.payload;
    },
    clearProfile: (state) => {
      state.currentProfile = null;
    },
  },
});

export const { setProfile, clearProfile } = profileSlice.actions;

export default profileSlice.reducer;
