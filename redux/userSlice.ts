import { createSlice } from "@reduxjs/toolkit";

interface UserState {
  currentUser: User | null;
}

interface User {
  token: string;
  email: string;
  avatar: string | null;
  first_name: string | null;
  id: number | null;
  last_name: string | null;
}

const initialState: UserState = {
  currentUser: null,
};

const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    setUser: (state, action) => {
      state.currentUser = action.payload;
    },
    clearUser: (state) => {
      state.currentUser = null;
    },
  },
});

export const { setUser, clearUser } = userSlice.actions;

export default userSlice.reducer;
