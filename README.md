# task-manager app

**task-manager** is a simple application built with Next.js that allows you to login & create and manage multiple areas with drag-and-drop functionality. You can add unique items to each area, and the app will prevent adding duplicate items.

## Features

- Login user (Integration with fake data api: "https://reqres.in/")
- Create multiple areas with drag-and-drop functionality.
- Add unique items to each area.
- Prevent duplicate items within an area.
- Intuitive and interactive user interface.

## Getting Started

Follow these steps to get the app up and running:

1. **Clone the repository:**

   ```sh
   git clone https://gitlab.com/dacost.dev/task-manager
   ```

First, run the development server:

```bash
npm run dev
# or
yarn dev
# or
pnpm dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

You can start editing the page by modifying `app/page.tsx`. The page auto-updates as you edit the file.

This project uses [`next/font`](https://nextjs.org/docs/basic-features/font-optimization) to automatically optimize and load Inter, a custom Google Font.

## Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.

You can check out [the Next.js GitHub repository](https://github.com/vercel/next.js/) - your feedback and contributions are welcome!

## Login credentials

```bash
{
    "email": "eve.holt@reqres.in",
    "password": "cityslicka"
}
```

- Visit for more details (https://reqres.in/)

## Technologies Used

- React: A JavaScript library for building user interfaces.
- Next.js: A React framework for server-side rendering and more.
- Redux: A predictable state container for JavaScript apps.
- TypeScript: A superset of JavaScript that adds static types.

## Contributing

Contributions are welcome! If you find a bug or have a suggestion, please open an issue or submit a pull request.

## License

This project is licensed under the MIT License.
